Router.route('/', function () {
  this.render('Home');
});

Router.route('/test', function () {
  this.render('test');
});

Router.route('/content1', function () {
    this.render('content1');
  }, {
    name: 'content1'
  }
);

Router.route('/content2', function () {
    this.render('content2');
  }, {
    name: 'content2'
  }
);

Router.configure({
  layoutTemplate: 'layout'
})
